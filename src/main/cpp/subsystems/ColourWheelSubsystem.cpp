/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#include "subsystems/ColourWheelSubsystem.h"

#include "Robot.h"
#include <iostream>



ColourWheelSubsystem::ColourWheelSubsystem() {

 
 
  exampleServo.SetBounds(2.0,1.8,1.5,1.2,1.0);
 

}


     

// This method will be called once per scheduler run
void ColourWheelSubsystem::Periodic() {
}


void ColourWheelSubsystem::ColourWheelFRD() {

// colourwheelmotor.Set(ControlMode::PercentOutput,0.1);
//exampleServo.Set(0.17);
//exampleServo.SetRaw(1.0);
exampleServo.SetSpeed(-1.0);

//exampleServo.setMaxPWM();
//exampleServo.SetPosition(1.0);
//exampleServo.SetAngle(3);
//exampleServo.Set(0.0);

 std::cout << " Value Set is " << exampleServo.Get() << " " << std::endl;
}

void ColourWheelSubsystem::ColourWheelREV() {
 //exampleServo.Set(0.0);
 exampleServo.SetSpeed(1.0);
 std::cout << " Value Set is " << exampleServo.Get() << " "  << std::endl;

//colourwheelmotor.Set(ControlMode::PercentOutput,-0.1);
//exampleServo.SetRaw(2.0);

//exampleServo.setMinPWM();
//exampleServo.SetPosition(0);
//exampleServo.SetAngle(169);
}

void ColourWheelSubsystem::ColourWheelSTOP() {
//colourwheelmotor.Set(ControlMode::PercentOutput,0.0);
//exampleServo.SetAngle(169);
//exampleServo.SetSpeed(1.0);
//exampleServo.Set(1.0);
 std::cout << " Value Set is " << exampleServo.Get() << " " << std::endl;

}

