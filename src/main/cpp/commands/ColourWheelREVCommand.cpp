/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#include "commands/ColourWheelREVCommand.h"
#include "Robot.h"  //mdd addded this.
#include <iostream>

ColourWheelREVCommand::ColourWheelREVCommand
(ColourWheelSubsystem* colourwheelsubsystem) 
 : m_colourwheelsubsystem(colourwheelsubsystem) {
  // Use addRequirements() here to declare subsystem dependencies.
}

// Called when the command is initially scheduled.
void ColourWheelREVCommand::Initialize() {

std::cout << " hello REV " << std::endl;
   m_colourwheelsubsystem->ColourWheelREV();

}

// Called repeatedly when this Command is scheduled to run
void ColourWheelREVCommand::Execute() {
std::cout << " hello REV " << std::endl;
  // m_colourwheelsubsystem->ColourWheelREV();

}

// Called once the command ends or is interrupted.
void ColourWheelREVCommand::End(bool interrupted) {

   m_colourwheelsubsystem->ColourWheelSTOP();
}

// Returns true when the command should end.
bool ColourWheelREVCommand::IsFinished() { return true; }
