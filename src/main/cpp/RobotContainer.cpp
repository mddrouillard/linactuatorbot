/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#include "RobotContainer.h"

#include <frc2/command/button/JoystickButton.h>
#include "commands/ColourWheelFRDCommand.h"
#include "commands/ColourWheelREVCommand.h"


RobotContainer::RobotContainer() : m_autonomousCommand(&m_subsystem) {
  // Initialize all of your commands and subsystems here

  // Configure the button bindings
  ConfigureButtonBindings();
}

void RobotContainer::ConfigureButtonBindings() {
  // Configure your button bindings here

   frc2::Button XB2b2{[&] { return m_XBOXController2.GetRawButton(2); }};
  frc2::Button XB2b4{[&] { return m_XBOXController2.GetRawButton(4); }};
 XB2b2.WhenPressed(ColourWheelFRDCommand(&m_colourwheelsubsystem));
  XB2b4.WhenPressed(ColourWheelREVCommand(&m_colourwheelsubsystem));
 
}

frc2::Command* RobotContainer::GetAutonomousCommand() {
  // An example command will be run in autonomous
  return &m_autonomousCommand;
}
